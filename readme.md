# INI

read, write ini config


## new ini and write file

```go
newini, err := ini.NewINI()
if err != nil {
    log.Fatal(err)
}

newini.Set("key", "val")
newini.SetFromSection("newone", "hello", "world")

f, err := os.Create("./new.ini")
defer f.Close()

newini.Write(writer)
```

## update exist ini and write file

```go
newini, err := ini.ParseFile("./test.ini")
if err != nil {
    log.Fatal(err)
}

newini.Set("key", "val")
newini.SetFromSection("newone", "hello", "world")

f, err := os.Create("./new.ini")
defer f.Close()

newini.Write(writer)
```

## write ini to buffer

```go
newini, err := ini.NewINI()
if err != nil {
    log.Fatal(err)
}

newini.Set("key", "val")
newini.SetFromSection("newone", "hello", "world")

var buf bytes.Buffer
writer := bufio.NewWriter(&buf)

newini.Write(writer)

// print out buff content
fmt.Printf("%s\n", buf.String())
```