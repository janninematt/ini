package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"

	"gitlab.com/janninematt/ini"
)

func main() {
	newini, err := ini.NewINI()
	if err != nil {
		log.Fatal(err)
	}
	// fmt.Println(newini)
	// fmt.Println(newini.Get("name"))
	f, err := os.Create("./new.ini")
	defer f.Close()

	var buf bytes.Buffer
	writer := bufio.NewWriter(&buf)

	if err := newini.SetFromSection("newone", "hello", "world"); err != nil {
		log.Fatal(err)
	}

	if err != nil {
		log.Fatalf("could not create file: %v", err)
	}

	newini.Write(writer)
	newini.Write(f)
	fmt.Printf("%s\n", buf.String())
}
