package ini

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
)

var (
	defaultSplitSign   = "\n"
	defaultSection     = ""
	defaultAssignSign  = "="
	defaultCommentSign = ';'
)

// Kvmap :key value map
type Kvmap map[string]string

// SectionMap :section => key value map
type SectionMap map[string]Kvmap

// INI : the ini file description
type INI struct {
	section SectionMap
}

func (ini *INI) newSection(sectionName string) error {
	if _, exist := ini.section[sectionName]; exist {
		return fmt.Errorf("duplicate section: %s", sectionName)
	}
	ini.section[sectionName] = make(Kvmap)
	return nil
}

// Reset : clear all section
func (ini *INI) Reset() {
	ini.section = make(SectionMap)
	ini.section[defaultSection] = make(Kvmap)
}

// Get : get value from default section
func (ini *INI) Get(key string) (value string, exist bool) {
	return ini.GetFromSection(defaultSection, key)
}

func (ini *INI) Set(key, value string) {
	ini.SetFromSection(defaultSection, key, value)
}

func (ini *INI) Delete(key string) {
	ini.DeleteFromSection(defaultSection, key)
}

func (ini *INI) GetKvMap(sectionName string) (Kvmap, bool) {
	kvmap, exist := ini.section[sectionName]
	return kvmap, exist
}

func (ini *INI) GetFromSection(sectionName string, key string) (value string, exist bool) {
	if section, exist := ini.section[sectionName]; exist {
		value, exist = section[key]
		return value, exist
	}
	return
}

func (ini *INI) SetFromSection(sectionName string, key string, value string) error {
	section, exist := ini.section[sectionName]
	if !exist {
		ini.newSection(sectionName)
		section = ini.section[sectionName]
	}
	section[key] = value
	return nil
}

func (ini *INI) DeleteFromSection(sectionName string, key string) {
	section, exist := ini.section[sectionName]
	if exist {
		delete(section, key)
	}
}

func (ini *INI) writeSectionKeyValue(section Kvmap, buf *bufio.Writer) {
	for key, value := range section {
		buf.WriteString(key + defaultAssignSign + value + defaultSplitSign)
	}
}

func (ini *INI) Write(w io.Writer) error {
	writer := bufio.NewWriter(w)
	if section, exist := ini.GetKvMap(defaultSection); exist {
		ini.writeSectionKeyValue(section, writer)
	} else {
		panic("default section is not exist")
	}
	for sectionName, section := range ini.section {
		if sectionName == defaultSection {
			continue
		}
		writer.WriteString("[" + sectionName + "]" + defaultSplitSign)
		ini.writeSectionKeyValue(section, writer)
	}
	return writer.Flush()
}

// NewINI : create a new ini
func NewINI() (*INI, error) {
	ini := &INI{
		section: make(SectionMap),
	}
	ini.section[defaultSection] = make(Kvmap)
	return ini, nil
}

func isSectionLine(line []byte) bool {
	return line[0] == '[' && line[len(line)-1] == ']'
}

func isKeyValueLine(line []byte) bool {
	return bytes.Index(line, []byte(defaultAssignSign)) >= 0
}

func isCommentLine(line []byte) bool {
	return line[0] == byte(defaultCommentSign)
}

func parse(data []byte) (*INI, error) {
	ini, _ := NewINI()
	currentSectionName := defaultSection

	lines := bytes.Split(data, []byte(defaultSplitSign))
	for lineno, line := range lines {
		if len(line) > 0 {
			line = bytes.TrimSpace(line)
			switch {
			case isCommentLine(line):
				continue
			case isSectionLine(line):
				currentSectionName = string(bytes.TrimSpace(line[1 : len(line)-1]))
				if err := ini.newSection(currentSectionName); err != nil {
					return nil, err
				}
			case isKeyValueLine(line):
				pos := bytes.Index(line, []byte(defaultAssignSign))
				key := string(bytes.TrimSpace(line[:pos]))
				val := string(bytes.TrimSpace(line[pos+len(defaultAssignSign):]))
				if kvmap, exist := ini.section[currentSectionName]; exist {
					kvmap[key] = val
				} else {
					panic(fmt.Sprintf("access section: %s is not exist", currentSectionName))
				}
			default:
				return nil, fmt.Errorf("line %d: invalid format: %v", lineno+1, line)
			}
		}
	}
	// fmt.Printf("ini: %v\n", ini)
	return ini, nil
}

// Parse : parse ini content from any io.Reader
func Parse(in io.Reader) (*INI, error) {
	data, err := ioutil.ReadAll(in)
	if err != nil {
		return nil, fmt.Errorf("could not read file: %v", err)
	}
	return parse(data)
}

// ParseFile : parse ini content from file
func ParseFile(filename string) (*INI, error) {
	f, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("could not read file: %v", err)
	}
	return parse(f)
}
